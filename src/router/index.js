import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/mainPage'
import DetailPage from '@/components/detailPage'
import axios from 'axios'

Vue.use(Router)
axios.defaults.baseURL = 'http://comento.cafe24.com'

Vue.prototype.$http = axios

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'main',
      component: Main
    },
    {
      path: '/detail',
      name: 'detailpage',
      component: DetailPage
    }
  ]
})
